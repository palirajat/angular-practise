import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpListComponent } from './emp-list/emp-list.component';
import { DepttListComponent } from './deptt-list/deptt-list.component';

const routes: Routes = [
  {path:'employees',component:EmpListComponent},
  {path:'departments',component:DepttListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents =[EmpListComponent,DepttListComponent]