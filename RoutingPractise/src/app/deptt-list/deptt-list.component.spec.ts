import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepttListComponent } from './deptt-list.component';

describe('DepttListComponent', () => {
  let component: DepttListComponent;
  let fixture: ComponentFixture<DepttListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepttListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepttListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
