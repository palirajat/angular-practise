import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  template: `<button (click)="onClick()">Click Me</button>
  <br>{{msg}}<br>

  <button (click)=" val=1000 ">Reset</button>

  `,
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {
 msg:number=2;
 val:number=1;
  constructor() { }

  ngOnInit() {
  }

  public onClick()
  {
    this.msg=this.val;
    this.val++;
  }

}
