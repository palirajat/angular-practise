import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  template: `<h1>hii {{firstName+" "+lastName+" "+firstName.length}}</h1>
  <h2>{{getUrl()}}</h2>
  <h3>{{age()}}</h3>
  <input type="text" [value]="id" name="id" [disabled]="false">`,
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  public firstName='Rajat';
  lastName='Pali';
  public id=" id from component";
  constructor() { }

  ngOnInit() {
  }

  getUrl()
  {
    return window.location.href;
  }

  public age() : number{
    return 23;
  }

}
