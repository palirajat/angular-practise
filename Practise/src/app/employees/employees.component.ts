import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  public empList;
  public empHttpList;
  public errorMessage;
  constructor(public empService:EmployeeService) {
    this.empList=empService.getEmpData();
    this.empService.geHttpEmpData().subscribe((data:any) =>
    {
      this.empHttpList=data;
      console.log(">>>>>>>>>"+this.empHttpList)
    },
    (error)=>{
      this.errorMessage=error;
    });
   }

  ngOnInit() {
  }

}
