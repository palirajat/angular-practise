import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-binding',
  template: `
  <h1 class="text-danger">this is text without class binding </h1>
  <h1 [class]="speacialClass">this is text with class binding </h1>
  <h1 class="text-success" [class]="speacialClass">this is text with class binding as well as normal </h1>
  <h2 [class.text-danger]="hasError">conditional Based class</h2>
  <h3 [ngClass]="multipleClasses">here we apply multiple classes on the bases of multiple conditions</h3>
  <h3 id="{{speacialClass}}" [ngClass]="speacialClass">here we apply multiple classes on the bases of multiple conditions</h3>
  <h4 [ngStyle]="styles">working of ng style</h4>
  `,
  styles: [`
   .text-success{ color:yellow; }
   .text-danger{ color:red; }
   .text-speacial{ font-style:italic; }
  `]
})
export class ClassBindingComponent implements OnInit {

  speacialClass:any = "text-speacial";
  speacialClass1:any = "text-success";
  hasError:boolean =true;
  multipleClasses={
    "text-success":this.hasError,
    "text-danger":!this.hasError,
    "text-speacial":this.hasError
  }
  styles={
    "color":"pink",
    "fontStyle":"italic"
  }
  constructor() { }

  ngOnInit() {
  }

}
