 import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { ClassBindingComponent } from './class-binding/class-binding.component';
import { EventBindingComponent } from './event-binding/event-binding.component';
import { TemplaterefvarComponent } from './templaterefvar/templaterefvar.component';
import { TwoWayBindingComponent } from './two-way-binding/two-way-binding.component';
import { StructDirectivesComponent } from './struct-directives/struct-directives.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { PipesComponent } from './pipes/pipes.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeService } from './services/employee.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    ClassBindingComponent,
    EventBindingComponent,
    TemplaterefvarComponent,
    TwoWayBindingComponent,
    StructDirectivesComponent,
    ParentComponent,
    ChildComponent,
    PipesComponent,
    EmployeesComponent
  ],
  imports: [
    BrowserModule,FormsModule,HttpClientModule
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
