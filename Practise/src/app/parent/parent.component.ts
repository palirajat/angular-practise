import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {
 public name="pali";
 public completeName="";
  constructor() { }
  
  ngOnInit() {
  }

  public setCompleteName(data:any)
  {

   this.completeName=data+" "+this.name;
    console.log(">>>>>>>>>>>data>>>>>>>>>>"+data)
  }
}
