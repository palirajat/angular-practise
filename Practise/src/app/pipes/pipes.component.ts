import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  template: `
  <h1> {{title | titlecase}}</h1>
  <h1>{{data}}</h1>
  <h1>{{data | lowercase}}</h1>
  <h1>{{data | uppercase}}</h1>
  <h1> {{title | slice:3:title.length-1}}</h1>
  <h2>{{listData | json}}
  <h3>{{5.678 | number:'1.2-3'}}</h3>
  <h3>{{5.678 | number:'2.4-6'}}</h3>
  <h3>{{5.678 | number:'1.1-2'}}</h3>
  <h3>{{5.678 | number:'10.2-3'}}</h3>
  <h4>{{0.25 | percent}}</h4>
  <h4>{{2 | percent}}</h4>
  <h4>{{1 | percent}}</h4>
  <h4>{{10 | currency :'INR'}}</h4>
  {{date}} <br>   {{date | date:'short'}} <br>  {{date| date:'shortTime'}} <br>   {{date| date:'shortDate'}} 
  `,
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {
  data="Rajat Pali Amritsar";
  title='Welcome to practise';
  public date =new Date();
  public listData={
    "name":"Rajat Pali",
    "age":18,
    "homeTown":"Amritsar"
  }
  constructor() { }

  ngOnInit() {
  }

}
