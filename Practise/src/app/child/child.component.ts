import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
@Input() public userLastName;
//@Input('userLastName ') public lastName;
@Output() public childEvent= new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  public submitData()
  {
    this.childEvent.emit('hey from rajat'); 
  }
}
