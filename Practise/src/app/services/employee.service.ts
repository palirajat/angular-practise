import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IEmployee } from '../employees/employee';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'
@Injectable()
export class EmployeeService {

  public url_string="/assets/data/employeeeeeeeeeeee.json"
  public employess=[
  {'name':'rajat pali','salary':'200000','exp':'3'},
  {'name':'vassu pali','salary':'500000','exp':'5'},
  {'name':'rohit pali','salary':'30000000','exp':'7'}]
  constructor( private http: HttpClient) { }

  public getEmpData():any
  {
    return this.employess;
  }

  public geHttpEmpData():Observable<IEmployee[]>
  {
    return this.http.get<IEmployee[]>(this.url_string).catch(this.errorHandling);
  }
  errorHandling(error:HttpErrorResponse)
  {
    return Observable.throw(error.message || 'Server error');
  }
} 
