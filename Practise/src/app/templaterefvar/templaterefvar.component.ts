import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templaterefvar',
  templateUrl: './templaterefvar.component.html',
  styleUrls: ['./templaterefvar.component.css']
})
export class TemplaterefvarComponent implements OnInit {
  value="";
  constructor() { }

  ngOnInit() {
  }
  public getValue(value)
  {
    this.value=value;
  }

}
