import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplaterefvarComponent } from './templaterefvar.component';

describe('TemplaterefvarComponent', () => {
  let component: TemplaterefvarComponent;
  let fixture: ComponentFixture<TemplaterefvarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplaterefvarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplaterefvarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
