import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-way-binding',
  template: `
  <input type="text" [(ngModel)]="name">
  {{name}}
  `,
    styleUrls: ['./two-way-binding.component.css']
})
export class TwoWayBindingComponent implements OnInit {
 name:any="";
  constructor() { }

  ngOnInit() {
  }

}
